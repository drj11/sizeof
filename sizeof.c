#include <assert.h>
#include <limits.h>
#include <stdio.h>

int main(void) {
    size_t i = sizeof(int);
    size_t l = sizeof(long);
    size_t ll = sizeof(long long);
    size_t p = sizeof(void *);

    assert(i <= INT_MAX/CHAR_BIT);
    assert(l <= INT_MAX/CHAR_BIT);
    assert(ll <= INT_MAX/CHAR_BIT);
    assert(p <= INT_MAX/CHAR_BIT);

    i *= CHAR_BIT;
    l *= CHAR_BIT;
    ll *= CHAR_BIT;
    p *= CHAR_BIT;

    printf("I%zd L%zd LL%zd P%zd\n", i, l, ll, p);
    return 0;
}
